# fa-criteria-backtest

Backtest FA criteria hits using ToS API

## Requirements

- [docker](https://docs.docker.com/install/)
- [docker-compose](https://docs.docker.com/compose/install/)

## Install

1. Create an account on https://developer.tdameritrade.com/
2. Create a new app on https://developer.tdameritrade.com/user/me/apps
   For the callback url, use http://127.0.0.1:8081/callback
3. Clone the fa-criteria-backtest git repo
4. Copy .env.example to .env
5. Set `TDA_CONSUMER_KEY` in .env to the key shown in the developer.tdameritrade.com
   console where you created your app.
6. Run `docker-compose up -d`
7. Run a backtest command:
   `docker-compose exec php php backtest.php <symbol> <buy date/time>`
   ex. `docker-compose exec php php backtest.php .SPY200501C282 '2020-04-27 00:00:00'`

Note, the first time you run a backtest, you will have to log in to your TDA account
and authorize your developer token to access the API.

## License

Apache2

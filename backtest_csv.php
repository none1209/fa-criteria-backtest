<?php

$filename = $argv[1];
$ext = pathinfo($argv[1], PATHINFO_EXTENSION);
$outfile = $argv[2] ?? pathinfo($argv[1], PATHINFO_DIRNAME).'/'.
                       pathinfo($argv[1], PATHINFO_FILENAME).
                       (empty($ext) ? '.out' : ".out.$ext");

echo "Writing output to $outfile\n";

require('vendor/autoload.php');

$dotenv = \Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();
$dotenv->required('TDA_CONSUMER_KEY')->notEmpty();

$auth = new App\ThinkOrSwimOauth(getenv('TDA_CONSUMER_KEY'));
$access_token = $auth->doLogin();

$tos = new App\ThinkOrSwimAPI($access_token);

$range = getenv('RANGE') ?: 'MONTH3';
$aggregation = getenv('AGGREGATE') ?: 'MIN5';

$memcached = new Clickalicious\Memcached\Client(
    getenv('MEMCACHED_HOST') ?: '127.0.0.1'
);

$fp = fopen($filename, 'r');
$out_fp = fopen($outfile, 'w');

// First CSV line is the header
$header = fgetcsv($fp);
fputcsv($out_fp, array_merge(
    $header,
    array('Option Symbol', 'Profitable', 'Buy Price', 'Max Profit Date/Time', 'Max Profit Sell Price')
));

$tz = new \DateTimeZone('US/Eastern');
$bt = new App\RunBacktest;
$i=1;
while (($data = fgetcsv($fp)) !== FALSE) {
    $data = array_combine($header, $data);
    $fa_date = new \DateTime("{$data['Date']} {$data['Time']}", $tz);
    $buy_ts = $fa_date->format('U');

    $option_date = new \DateTime($data['Expiry'], $tz);
    $option_date = $option_date->format('ymd');
    $option_symbol =
        '.'.$data['Ticker'].
        $option_date.
        substr(ucfirst($data['C/P']), 0, 1).
        $data['Strike'];
    $option_symbol = preg_replace('/[^\.A-Za-z0-9]/', '', $option_symbol);
    $data['option_symbol'] = $option_symbol;
    echo "Processing $i: {$data['option_symbol']}\n";
    $i++;

    if (($chart = $memcached->get($option_symbol)) === FALSE) {
        $chart = $tos->getChartData($option_symbol, $range, $aggregation);
        $memcached->set($option_symbol, $chart, 21600);
    }
    $bt_result = $bt->backtest_trade($chart, $buy_ts);
    $data['Profitable'] = (empty($bt_result['profitable_exits']) ? 'NO' : 'YES');

    if ($data['Profitable'] == 'NO') {
        fputcsv($out_fp, $data);
        continue;
    }

    $data['buy_price'] = $bt_result['buy_price'];
    $max_profit_sell = $bt->sort_by_profit($bt_result['profitable_exits'])[0];
    $max_profit_ts = (new \DateTime(null, $tz))->setTimestamp($max_profit_sell['timestamp']);
    $data['max_profit_ts'] = $max_profit_ts->format('Y-m-d H:i:s');
    $data['max_profit_sell_price'] = $max_profit_sell['high'];
    fputcsv($out_fp, $data);
}
?>

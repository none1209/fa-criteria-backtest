<?php

$symbol = $argv[1];
$buy_time = $argv[2];

require('vendor/autoload.php');

$dotenv = \Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();
$dotenv->required('TDA_CONSUMER_KEY')->notEmpty();

$auth = new App\ThinkOrSwimOauth(getenv('TDA_CONSUMER_KEY'));
$access_token = $auth->doLogin();

$tos = new App\ThinkOrSwimAPI($access_token);

$range = getenv('RANGE') ?: 'MONTH3';
$aggregation = getenv('AGGREGATE') ?: 'MIN5';
$chart = $tos->getChartData($symbol, $range, $aggregation);

$date = new \DateTime($buy_time, new DateTimeZone('US/Eastern'));
$buy_ts = $date->format('U');

$bt = new App\RunBacktest;
$bt_result = $bt->backtest_trade($chart, $buy_ts);
$bt->print_backtest_report($bt_result);

?>

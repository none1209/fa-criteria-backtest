<?php
namespace App\Exceptions;

class ToSLoginFailedException extends \Exception {
    public function __construct($message = 'ToS websocket login failed', $code = 0, \Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}
?>

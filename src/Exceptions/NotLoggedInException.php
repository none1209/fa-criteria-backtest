<?php
namespace App\Exceptions;

class NotLoggedInException extends \Exception {
    public function __construct($message = 'ToS connection is not logged in', $code = 0, \Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}
?>

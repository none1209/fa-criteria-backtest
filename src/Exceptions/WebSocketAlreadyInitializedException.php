<?php
namespace App\Exceptions;

class WebSocketAlreadyInitializedException extends \Exception {
    public function __construct($message = 'WebSocket is already initialized', $code = 0, \Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}
?>

<?php
namespace App;

use Exceptions\NotLoggedInException;
use Exceptions\ToSLoginFailedException;
use Exceptions\WebSocketAlreadyInitializedException;
use WebSocket\Client;

class ThinkOrSwimAPI {
    protected $wss_uri = 'wss://services.thinkorswim.com/Services/WsJson';
    protected $conn;  // ToS websocket connection
    protected $session_id;
    protected $access_token;
    protected $logged_in = false;

    function __construct($access_token = null) {
        // Passing an access token will attempt to log in immediately
        if (!is_null($access_token)) {
            $this->setAccessToken($access_token);
            $this->initWebsocketSession();
            $this->loginToS();
        }

        return true;
    }

    function setWssUrl($uri) {
        if ($this->logged_in) {
            throw new Exception('Cannot change Websocket URI of logged in session');
        }

        $this->wss_uri = $uri;
    }

    function setAccessToken($access_token) {
        if ($this->logged_in) {
            throw new Exception('Cannot change access token of logged in session');
        }

        $this->access_token = $access_token;
    }

    function initWebsocketSession() {
        if (!is_null($this->conn)) throw new WebSocketAlreadyInitializedException;
        $this->conn = new Client($this->wss_uri, array('timeout' => 60));
        $init = array(
            "ver" => "25.*.*",
            "fmt" => "json-patches",
        );
        $this->conn->send(json_encode($init));
        $response = json_decode($this->conn->receive());
        if (!isset($response->session)) {
            throw new ToSLoginFailedException('Failed to start ToS session');
        }

        $this->session_id = $response->session;
        return true;
    }

    function loginToS() {
        if (is_null($this->conn)) $this->initWebsocketSession();
        if (is_null($this->access_token)) throw new ToSLoginFailedException('access_token missing');

        $login = array(
            "payload" => array(
                array(
                    "service" => "login",
                    "id" => "login",
                    "ver" => 0,
                    "domain" => "TOS",
                    "platform" => "PROD",
                    "accessToken" => $this->access_token
                )
            )
        );

        $this->conn->send(json_encode($login));
        $response = json_decode($this->conn->receive());
        if ($response
                ->payloadPatches[0]
                ->patches[0]
                ->value
                ->authenticationStatus !== 'OK') {
            throw new ToSLoginFailedException;
        }

        $this->logged_in = true;

        return true;
    }

    function getChartData($symbol, $range = null, $aggregationPeriod = null) {
        /*
        * Symbol for options in ToS is like .SPY200501C283
        * ie. .<underlying symbol>YYmmdd<C or P><strike>
        * date('ymd') for the date string
        * aggregationPeriod can be something like MIN1 (1 minute aggregation)
        * range is the scope of data to capture starting from present
        * range can be something like YEAR1, DAY15 (15 days), MONTH2 (2 months), etc.
        */
        $request = array(
            "payload" => array(
                array(
                    "service" => "chart",
                    "id" => "chart",
                    "ver" => 0,
                    "symbol" => $symbol,
                    "aggregationPeriod" => $aggregationPeriod ?? "MIN1",
                    "range" => $range ?? "DAY1"
                )
            )
        );

        $this->conn->send(json_encode($request));
        $response = json_decode($this->conn->receive());
        $response = $response->payloadPatches[0]->patches[0]->value;
        $zipped_ticks = array_map(function (...$tick) {
                return array(
                    'timestamp' => $tick[0],
                    'open' => $tick[1],
                    'low' => $tick[2],
                    'high' => $tick[3],
                    'close' => $tick[4],
                    'volume' => $tick[5]
                );
            },
            $response->timestamps,
            $response->open,
            $response->low,
            $response->high,
            $response->close,
            $response->volume);

        // Time sort the ticks
        usort($zipped_ticks, function($a, $b) {
            return $a['timestamp'] - $b['timestamp'];
        });

        return array(
            'symbol' => $response->symbol,
            'instrument' => $response->instrument,
            'ticks' => $zipped_ticks
        );
    }
}

?>

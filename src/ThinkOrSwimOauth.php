<?php
namespace App;

use \GuzzleHttp\Client;

class ThinkOrSwimOauth {
    // Based on https://developer.okta.com/blog/2018/07/16/oauth-2-command-line
    // and https://github.com/aaronpk/command-line-oauth/blob/master/login.php
    protected $token_url = 'https://api.tdameritrade.com/v1/oauth2/token';
    protected $ip;
    protected $port;
    protected $state;
    protected $consumer_key;
    protected $client_id;
    protected $redirect_uri;
    protected $auth_code;
    protected $refresh_token;
    protected $token_cache_file;

    function __construct($consumer_key, $token_cache_file = null) {
        $this->ip = getenv('OAUTH_CALLBACK_BIND_IP') ?: '127.0.0.1';
        $this->port = getenv('OAUTH_CALLBACK_BIND_PORT') ?: '8080';
        $this->state = bin2hex(random_bytes(5));
        $this->consumer_key = $consumer_key;
        $this->client_id = $this->consumer_key.'@AMER.OAUTHAP';
        $this->redirect_uri = getenv('OAUTH_CALLBACK_URI') ?: "http://{$this->ip}:{$this->port}/callback";

        $this->token_cache_file = $token_cache_file ?? dirname(dirname(__FILE__)).'/.token';
    }

    function doLogin() {
        if (file_exists($this->token_cache_file)) {
            // A refresh token should be present already, try to use it
            $token_cache = $this->loadTokenCache();
            $this->setRefreshToken($token_cache->refresh_token);
            $tokens = $this->getTokenWithRefreshToken();
            $this->updateTokenCache($tokens);
            return $tokens->access_token;
        }

        $this->doWebLogin();
        $tokens = $this->getTokenWithAuthCode();
        $this->updateTokenCache($tokens);
        return $tokens->access_token;
    }

    protected function loadTokenCache() {
        return json_decode(file_get_contents($this->token_cache_file));
    }

    protected function updateTokenCache($tokens) {
        file_put_contents($this->token_cache_file, json_encode($tokens));
    }

    function doWebLogin() {
        $authorize_url = 'https://auth.tdameritrade.com/auth?'.http_build_query([
            'response_type' => 'code',
            'redirect_uri' => $this->redirect_uri,
            'client_id' => $this->client_id,
            'state' => $this->state,
        ]);

        echo "Open the following URL in a browser to continue\n";
        echo $authorize_url."\n";
        shell_exec('open '.escapeshellarg($authorize_url));

        $auth = $this->startHttpServer("tcp://{$this->ip}:{$this->port}");
        if ($auth['state'] != $this->state) throw new \Exception('Login failed, wrong state token returned');

        $this->setAuthCode($auth['code']);
    }

    function setAuthCode($code) {
        $this->auth_code = $code;
    }

    function setRefreshToken($token) {
        $this->refresh_token = $token;
    }

    protected function getToken($params) {
        $params['client_id'] = $this->client_id;
        $params['access_type'] = 'offline';
        $params['redirect_uri'] = $this->redirect_uri;
        $client = new Client();

        $response = $client->request(
            'POST',
            $this->token_url,
            ['form_params' => $params]
        );

        if ($response->getStatusCode() != 200) {
            throw new \Exception('Failed to fetch token: '.$response->getStatusCode().' '.$response->getReasonPhrase);
        }

        /* returns an array
         * ['access_token' => '<token>', 'refresh_token' => '<refresh_token>'
         *  'scope' => 'scopes', 'expires_in' => '<access token expiry>',
         *  'refresh_token_expires_in' => '<refresh token expiry>'
         *  'token_type' => 'Bearer']
         */
        $tokens = $response->getBody()->getContents();
        return json_decode($tokens);
    }

    function getTokenWithAuthCode() {
        if (is_null($this->auth_code)) throw new \Exception('No auth code is set');
        $params = array(
            'grant_type' => 'authorization_code',
            'code' => $this->auth_code,
        );

        $tokens = $this->getToken($params);
        return $tokens;
    }

    function getTokenWithRefreshToken() {
        if (is_null($this->refresh_token)) throw new \Exception('No refresh token is set');
        $params = array(
            'grant_type' => 'refresh_token',
            'refresh_token' => $this->refresh_token
        );

        $tokens = $this->getToken($params);
        return $tokens;
    }

    function startHttpServer($socketStr) {
        // Adapted from http://cweiske.de/shpub.htm

        $responseOk = "HTTP/1.0 200 OK\r\n"
          . "Content-Type: text/plain\r\n"
          . "\r\n"
          . "Ok. You may close this tab and return to the shell.\r\n";
        $responseErr = "HTTP/1.0 400 Bad Request\r\n"
          . "Content-Type: text/plain\r\n"
          . "\r\n"
          . "Bad Request\r\n";

        ini_set('default_socket_timeout', 60 * 5);

        $server = stream_socket_server($socketStr, $errno, $errstr);

        if(!$server) {
          Log::err('Error starting HTTP server');
          return false;
        }

        do {
          $sock = stream_socket_accept($server);
          if(!$sock) {
            Log::err('Error accepting socket connection');
            exit(1);
          }
          $headers = [];
          $body    = null;
          $content_length = 0;
          //read request headers
          while(false !== ($line = trim(fgets($sock)))) {
            if('' === $line) {
              break;
            }
            $regex = '#^Content-Length:\s*([[:digit:]]+)\s*$#i';
            if(preg_match($regex, $line, $matches)) {
              $content_length = (int)$matches[1];
            }
            $headers[] = $line;
          }
          // read content/body
          if($content_length > 0) {
            $body = fread($sock, $content_length);
          }
          // send response
          list($method, $url, $httpver) = explode(' ', $headers[0]);
          if($method == 'GET') {
            $parts = parse_url($url);
            if(isset($parts['path']) && $parts['path'] == '/callback'
              && isset($parts['query'])
            ) {
              parse_str($parts['query'], $query);
              if(isset($query['code']) && isset($query['state'])) {
                fwrite($sock, $responseOk);
                fclose($sock);
                return $query;
              }
            }
          }
          fwrite($sock, $responseErr);
          fclose($sock);
        } while (true);
      }

}

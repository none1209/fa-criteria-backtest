<?php
namespace App;

class RunBacktest {
    function backtest_trade($data, $buy_ts) {
        // Pass in symbol data from ToS Chart
        $return = array(
            'instrument' => $data['instrument'],
            'buy_time' => null,
            'buy_price' => null,
            'profitable_exits' => array()
        );
        foreach ($data['ticks'] as $tick) {
            // ToS returns microtime, convert to unixtime sceonds precision
            $tick['timestamp'] = $tick['timestamp'] / 1000;

            // Have not reached the buy point yet
            if ($tick['timestamp'] < $buy_ts) continue;
            else if (is_null($return['buy_time']) && $tick['timestamp'] >= $buy_ts) {
                $return['buy_time'] = $tick['timestamp'];
                $return['buy_price'] = (($tick['high'] + $tick['low']) / 2);
            }
            else {
                // Purchase occurred, now track profitable exits
                if ($tick['high'] > $return['buy_price']) {
                    array_push($return['profitable_exits'], $tick);
                }
            }
        }

        return $return;
    }

    function sort_by_profit($candles) {
        usort($candles, function($a, $b) {
            // Sort by most profitable
            $diff = $b['high'] - $a['high'];
            if (abs($diff) < PHP_FLOAT_EPSILON) return 0;  // Values are equal
            if ($diff > 0) return 1;
            if ($diff < 0) return -1;
        });
        return $candles;
    }

    function print_backtest_report($report, $exit_prints = 5) {
        // Print out the backtest results
        $date = new \DateTime(null, new \DateTimeZone('US/Eastern'));
        $date->setTimestamp($report['buy_time']);
        $buy_time = $date->format('Y-m-d H:i:s T');

        $inst = $report['instrument'];
        $inst_desc = "{$inst->symbol} - {$inst->underlyingSymbol} {$inst->expiration} {$inst->strike} {$inst->optionType}";
        echo "Backtest for $inst_desc\n";
        echo "Buy Time: {$buy_time}\nBuy Price: {$report['buy_price']}\n\n";
        if (empty($report['profitable_exits'])) {
            echo "No profitable exits found.\n";
            return;
        }

        $report['profitable_exits'] = $this->sort_by_profit($report['profitable_exits']);

        $top_count = (count($report['profitable_exits']) > $exit_prints ? $exit_prints : count($report['profitable_exits']));
        echo "Top $top_count exits:\n";
        foreach (array_slice($report['profitable_exits'], 0, $exit_prints) as $exit) {
            $date->setTimestamp($exit['timestamp']);
            $exit_ts = $date->format('Y-m-d H:i:s');
            $pct_profit = floor((($exit['high'] / $report['buy_price'])-1) * 100);
            echo "Sell {$exit_ts}: \${$exit['high']} ({$pct_profit}%)\n";
        }
    }

}
